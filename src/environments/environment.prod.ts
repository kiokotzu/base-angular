export const environment = {
  production: true,
  languages: {
    es: 'es',
    en: 'en'
  }
};
